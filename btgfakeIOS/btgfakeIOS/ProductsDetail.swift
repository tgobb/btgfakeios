//
//  ProductsDetail.swift
//  btgfakeIOS
//
//  Created by Tiago Moraes on 31/01/20.
//  Copyright © 2020 Tiago Moraes. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import AllInMobileSwift

class ProductsDetail: UIViewController {
    var productBrand : String? = nil
    var productName : String? = nil
    var productImage : String? = nil
    var productDesc : String? = nil
    var productsId : String? = nil
    
    @IBOutlet weak var lbProductName: UILabel!
    @IBOutlet weak var lbProductBrand: UILabel!
    @IBOutlet weak var imgProductImage: UIImageView!
    @IBOutlet weak var lbProductDesc: UILabel!
    
    
    @IBAction func btBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnWishList(_ sender: Any) {
        Toast.show(message: "Produto adicionado a lista de desejos!", controller: self)
        
        let wishObject = AIWish.init(productId: productsId!, active: true)
        BTG360.addWish(account: "60:1", wish: wishObject)
    }
    
    @IBAction func btnWarnMe(_ sender: Any) {
        Toast.show(message: "Entraremos em contato assim que o produto estiver disponível", controller: self)
        
        let warnObject = AIWarn.init(productId: productsId!, active: true)
        BTG360.addWarn(account: "60:1", warn: warnObject)
    }
    
    override func viewDidLoad() {
        let url = URL(string: productImage!)
        self.lbProductBrand.text = productBrand
        self.lbProductName.text = productName
        self.imgProductImage.kf.setImage(with: url)
        self.lbProductDesc.text = productDesc
    }
}
