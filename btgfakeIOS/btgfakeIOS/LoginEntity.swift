//
//  LoginEntity.swift
//  btgfakeIOS
//
//  Created by Tiago Moraes on 18/02/20.
//  Copyright © 2020 Tiago Moraes. All rights reserved.
//

import Foundation

struct LoginEntity: Codable {
    var email: String?
}
