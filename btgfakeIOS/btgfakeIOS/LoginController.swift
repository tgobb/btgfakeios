//
//  LoginController.swift
//  btgfakeIOS
//
//  Created by Tiago Moraes on 31/01/20.
//  Copyright © 2020 Tiago Moraes. All rights reserved.
//

import Foundation
import UIKit
import AllInMobileSwift

class LoginController: UIViewController {
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var tfEmail: UITextField!
    
    override func viewDidLoad() {
        
        btnLogin.backgroundColor = .black
        btnLogin.layer.cornerRadius = 5
        btnLogin.layer.borderWidth = 1
        btnLogin.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBAction func clickLogin(_ sender: Any) {
        
        let loginInfo = tfEmail.text
        
        let userEmail = AIClient.init(email: loginInfo!)
        
        BTG360.addClient(account: "60:1", client: userEmail)
    }
    
    
    @IBAction func btBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
