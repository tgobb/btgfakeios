//
//  FullProductsCell.swift
//  btgfakeIOS
//
//  Created by Tiago Moraes on 30/01/20.
//  Copyright © 2020 Tiago Moraes. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class FullProductsCell: UITableViewCell {
    @IBOutlet weak var lbProductName: UILabel!
    @IBOutlet weak var lbProductBrand: UILabel!
    @IBOutlet weak var imgProductImage: UIImageView!
    
    func setup (products : ProductsEntitiy) {
        
        let url = URL(string: products.image!)
        
        lbProductName.text = products.product_name
        lbProductBrand.text = products.brand
        imgProductImage.kf.setImage(with: url)
    }
}
