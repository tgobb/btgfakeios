//
//  File.swift
//  btgfakeIOS
//
//  Created by Tiago Moraes on 30/01/20.
//  Copyright © 2020 Tiago Moraes. All rights reserved.
//

import Foundation;
import Alamofire;

class ProductsService {
    
    func getProducts(onSuccess: @escaping ([ProductsEntitiy]) -> ()) {
        
        let url = "https://sale-attribution.allin.com.br/product/"
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, interceptor:nil).responseJSON { response in
            switch response.result {
            case .success(_):
                guard let jsonData = response.data, let productsList = try? JSONDecoder().decode([ProductsEntitiy].self, from: jsonData) else {
                    fallthrough
                }
                print(productsList)
                onSuccess(productsList)
            case .failure(_):
                print("Erro")
                }
            }
        }
    }
