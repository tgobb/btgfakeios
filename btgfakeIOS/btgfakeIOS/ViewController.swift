//
//  ViewController.swift
//  btgfakeIOS
//
//  Created by Tiago Moraes on 30/01/20.
//  Copyright © 2020 Tiago Moraes. All rights reserved.
//

import UIKit
import Foundation
import AllInMobileSwift

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tvProducts: UITableView!
    @IBOutlet weak var sbSearch: UISearchBar!
    @IBOutlet weak var btnLogin: UIButton!
    
    var productsId : String? = nil
    
    @IBAction func btnLogin(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "LoginController") as? UIViewController {
            
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    var productsArray = [ProductsEntitiy]()
    var filteredProducts = [ProductsEntitiy]()
    var searching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogin.backgroundColor = .black
        btnLogin.layer.cornerRadius = 5
        btnLogin.layer.borderWidth = 1
        btnLogin.layer.borderColor = UIColor.black.cgColor
        
        // Register the table view cell class and its reuse id
        self.tvProducts.register(UINib(nibName: "FullProductsCell", bundle: nil), forCellReuseIdentifier: "FullProductsCell")
        
        // This view controller itself will provide the delegate methods and row data for the table view.
        self.tvProducts.delegate = self
        self.tvProducts.dataSource = self
        
        
        
        let getProducts = ProductsService()
        getProducts.getProducts { (array) in
            self.productsArray = array
            self.tvProducts.reloadData()
        }
    }
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return filteredProducts.count
        }else {
            return productsArray.count
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tvProducts.dequeueReusableCell(withIdentifier: "FullProductsCell", for: indexPath) as! FullProductsCell
        
        if searching {
            cell.setup(products: filteredProducts[indexPath.row])
        } else {
            cell.setup(products: productsArray[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Cell cliked value is \(indexPath.row)")
            
        let productsDetail = self.productsArray[indexPath.row]
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "ProductsDetail") as? ProductsDetail {
            
            controller.productName = productsDetail.product_name
            controller.productBrand = productsDetail.brand
            controller.productImage = productsDetail.image
            controller.productDesc = productsDetail.product_desc
            controller.productsId = productsDetail.product_id
            
            self.present(controller, animated: true, completion: nil)
        }
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredProducts = self.productsArray.filter({$0.product_name!.prefix(searchText.count) == searchText})
        searching = true
        let searchObject = AISearch.init(keyword: searchText)
        BTG360.addSearch(account: "60:1", search: searchObject)
        tvProducts.reloadData()
    }
}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
