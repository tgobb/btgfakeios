//
//  ProductsEntity.swift
//  btgfakeIOS
//
//  Created by Tiago Moraes on 30/01/20.
//  Copyright © 2020 Tiago Moraes. All rights reserved.
//

import Foundation


struct ProductsEntitiy: Codable {
    var brand: String?
    var product_desc: String?
    var image: String?
    var product_name: String?
    var product_id: String?
}
